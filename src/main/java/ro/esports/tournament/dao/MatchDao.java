package ro.esports.tournament.dao;

import ro.esports.tournament.model.Match;

public interface MatchDao {
	Match saveMatch(Match match);
	Match getMatch(long id);
	void deleteMatch(Match match);
}
