package ro.esports.tournament.dao;

import ro.esports.tournament.model.Player;

public interface PlayerDao {
	Player savePlayer(Player player);
	Player getPlayer(long id);
	void deletePlayer(Player player);
}
