package ro.esports.tournament.dao;

import ro.esports.tournament.model.Team;

public interface TeamDao {
	
	Team saveTeam(Team team);
	Team getTeam(long id);
	void deleteTeam(Team team);
}
