package ro.esports.tournament.dao;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import ro.esports.tournament.model.Player;

public class PlayerDaoImpl implements PlayerDao {
	@Inject
	private EntityManager entityManager;

	public Player savePlayer(Player player) {
		entityManager.getTransaction().begin();
		Player playerToReturn = entityManager.merge(player);
		entityManager.getTransaction().commit();
		return playerToReturn;
	}

	public Player getPlayer(long id) {
		return entityManager.find(Player.class, id);
	}

	public void updatePlayer(Player player) {
		
	}

	public void deletePlayer(Player player) {
		entityManager.getTransaction().begin();
		entityManager.remove(player);
		entityManager.getTransaction().commit();
	}
}
