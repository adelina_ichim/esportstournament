package ro.esports.tournament;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.core.Application;

import ro.esports.tournament.service.TournamentRestService;

public class App extends Application{

	private Set<Class<?>> classes = new HashSet<>();

	public App() {
		classes.add(TournamentRestService.class);
	}

	@Override
	public Set<Class<?>> getClasses() {
		return classes;
	}
}
