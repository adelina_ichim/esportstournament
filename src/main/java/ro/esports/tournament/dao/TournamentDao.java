package ro.esports.tournament.dao;

import ro.esports.tournament.model.Tournament;

public interface TournamentDao {
	
	Tournament saveTournament(Tournament tournament);
	Tournament getTournament(long id);
	void deleteTournament(Tournament tournament);
}
