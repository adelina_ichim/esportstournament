package ro.esports.tournament.dao;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import ro.esports.tournament.model.Tournament;

public class TournamentDaoImpl implements TournamentDao {

	@Inject
	private EntityManager entityManager;

	public Tournament saveTournament(Tournament tournament) {

		entityManager.getTransaction().begin();
		Tournament tour = entityManager.merge(tournament);
		entityManager.getTransaction().commit();
		return tour;

	}

	public Tournament getTournament(long id) {
		return entityManager.find(Tournament.class, id);
	}

	public void deleteTournament(Tournament tournament) {
		entityManager.getTransaction().begin();
		entityManager.remove(tournament);
		entityManager.getTransaction().commit();
	}

	
}
