package ro.esports.tournament.dao;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import ro.esports.tournament.model.Team;

public class TeamDaoImpl implements TeamDao{
	
	@Inject
	private EntityManager entityManager;
	
	public Team saveTeam(Team team) {
		entityManager.getTransaction().begin();
		Team team1 = entityManager.merge(team);
		entityManager.getTransaction().commit();
		return team1;
	}

	public Team getTeam(long id) {
		return entityManager.find(Team.class, id);
	}

	public void deleteTeam(Team team) {
		entityManager.getTransaction().begin();
		entityManager.remove(team);
		entityManager.getTransaction().commit();		
	}

}
