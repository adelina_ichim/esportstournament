package ro.esports.tournament.dao;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import ro.esports.tournament.model.Match;

public class MatchDaoImpl implements MatchDao{

	@Inject
	private EntityManager entityManager;

	public Match saveMatch(Match match) {
		
		entityManager.getTransaction().begin();
		Match match1 = entityManager.merge(match);
		entityManager.getTransaction().commit();
		return match1;
	}

	public Match getMatch(long id) {
		return entityManager.find(Match.class, id);
	}

	public void deleteMatch(Match match) {
		entityManager.getTransaction().begin();
		entityManager.remove(match);
		entityManager.getTransaction().commit();
		
	}

}
