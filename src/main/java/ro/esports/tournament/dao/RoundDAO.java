package ro.esports.tournament.dao;

import ro.esports.tournament.model.Round;

public interface RoundDAO {

	Round saveRound(Round round);
	Round getRound(long id);
	void updateRound(Round round);
	void deleteRound(Round round);
}
