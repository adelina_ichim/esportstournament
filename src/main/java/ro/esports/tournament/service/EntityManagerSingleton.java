package ro.esports.tournament.service;

import javax.enterprise.inject.Produces;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class EntityManagerSingleton {

	private static EntityManagerFactory entityManagerFactory=null;
	private static EntityManager entityManager=null;
	
	@Produces
	public static EntityManager getEntityManager(){
		if(entityManager==null){
			entityManagerFactory = Persistence.createEntityManagerFactory("esports-pu");
			entityManager = entityManagerFactory.createEntityManager();
		}
		return entityManager;
	}
	public void destroy(EntityManagerFactory factory){
		factory.close();
	}
}
