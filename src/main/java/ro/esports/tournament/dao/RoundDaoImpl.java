package ro.esports.tournament.dao;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import ro.esports.tournament.model.Round;

public class RoundDaoImpl implements RoundDAO{
	
	@Inject
	private EntityManager entityManager;

	public Round saveRound(Round round) {
		entityManager.getTransaction().begin();
		Round roundToReturn = entityManager.merge(round);
		entityManager.getTransaction().commit();
		return roundToReturn;
	}

	public Round getRound(long id) {
		return entityManager.find(Round.class, id);
	}

	public void updateRound(Round round) {
		
	}

	public void deleteRound(Round round) {
		entityManager.getTransaction().begin();
		entityManager.remove(round);
		entityManager.getTransaction().commit();
	}
	

}
